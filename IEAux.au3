#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once

#include <IE.au3>

; получение объекта(-ов) по одному из следующих методов JS:
; querySelector, querySelectorAll, getElementById, getElementsByClassName
;
; входной объект - глобальный объект ИЕ или какой-либо элемент DOM
;
; если результирующий объект содержит несколько объектов то доступ к ним во вне -
; с помощью метода item, по индексному номеру элемента (считая от 0), например:
;     $oElements = _IEGetObjByJsMethod($obj, 'getElementsByClassName', 'someclass')
;     $oElement = $oElements.item(0)
;
; а число элементов по свойству length (как в JS)

Func _IEGetObjByJSMethod($sMethodName, $sMethodParameter, ByRef Const $oObject, _
		$bDocument = True, $iDelay = 200, $iRetriesLimit = 2)

	If Not IsObj($oObject) Then Return SetError(1)

	If $bDocument = Default Then $bDocument = True
	If $iDelay = Default Then $iDelay = 200
	If $iRetriesLimit = Default Then $iRetriesLimit = 2

	Local $oContainer

	If $bDocument Then

		$oContainer = $oObject.document
	Else
		$oContainer = $oObject
	EndIf

	Local $oRet
	Local $i = 0

	Do
		$i += 1
		If $i > $iRetriesLimit Then Return SetError(2)

		Switch $sMethodName

			Case 'querySelector'

				$oRet = $oContainer.querySelector($sMethodParameter)

			Case 'querySelectorAll'

				$oRet = $oContainer.querySelectorAll($sMethodParameter)

			Case 'getElementById'

				$oRet = $oContainer.getElementById($sMethodParameter)

			Case 'getElementsByClassName'

				$oRet = $oContainer.getElementsByClassName($sMethodParameter)

			Case Else
				Return SetError(3)
		EndSwitch

		Sleep($iDelay)

	Until IsObj($oRet)

	Return $oRet

EndFunc   ;==>_IEGetObjByJSMethod
