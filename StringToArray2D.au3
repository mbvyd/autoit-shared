#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once

; на основе https://www.autoitscript.com/forum/topic/148947-multi-dimensional-arrays-stringsplit-function/?do=findComment&comment=1060391

Func _StringToArray2D( _
		ByRef Const $sString, _
		ByRef $aRes, _
		$sDelim4Rows = @CRLF, _
		$sDelim4Cols = ';' _
		)

	Local $STR_ENTIRESPLIT = 1 ; флаг для StringSplit

	Local $aRows = StringSplit($sString, $sDelim4Rows, $STR_ENTIRESPLIT)

	If @error Then Return SetError(1)

	Local $aCols, $iCols = 0

	Dim $aRes[$aRows[0] + 1][0]

	For $i = 1 To $aRows[0]

		$aCols = StringSplit($aRows[$i], $sDelim4Cols, $STR_ENTIRESPLIT) ; разделяем каждый ряд на колонки

		If Not @error Then

			; при первой итерации условие выполняется всегда;
			; в последующем - если ряд разделяется на бОльшее число
			; элементов (колонки для 2д массива), чем предыдущий

			If $aCols[0] > $iCols Then

				; установить размерность 2д массива - для включения текущего числа колонок

				$iCols = $aCols[0]

				ReDim $aRes[$aRows[0] + 1][$iCols]
			EndIf
		EndIf

		For $j = 1 To $aCols[0] ; наполняем ряд 2д массива

			$aRes[$i][$j - 1] = $aCols[$j]
		Next
	Next

	If $iCols <= 1 Then ; 1 колонка означает, что не было разбиения на несколько

		$aRes = $aRows
		Return
	EndIf

	$aRes[0][0] = $aRows[0]
	$aRes[0][1] = UBound($aRes, 2) - 1

	If Not IsArray($aRes) Then Return SetError(2)

EndFunc   ;==>_StringToArray2D
