#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#include-once

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

 Script Function:
	класс словаря

#ce ----------------------------------------------------------------------------

#Region ; класс словаря

; конструктор

Func _Dictionary()

	Return ObjCreate("Scripting.Dictionary")

EndFunc   ;==>_Dictionary


; установка словаря в элементе одномерного массива

Func _Dictionary_SetInArray1D(ByRef $aArray, $i)

	If Not IsArray($aArray) Or $i >= UBound($aArray) Then Return

	$aArray[$i] = _Dictionary()

EndFunc   ;==>Dictionary_SetInArray


Func _Dictionary_IsValid(ByRef Const $oDictionary)

	Return ObjName($oDictionary) == 'Dictionary' ? True : False

EndFunc   ;==>_Dictionary_IsValid


Func _Dictionary_AddSafe(ByRef $oDictionary, $vKey, $vValue = Null)

	If Not $oDictionary.Exists($vKey) Then

		If $vValue = Null Then

			$oDictionary.Item($vKey)
		Else
			$oDictionary.Add($vKey, $vValue)
		EndIf
	EndIf
EndFunc   ;==>_Dictionary_AddSafe


Func _Dictionary_RemoveSafe(ByRef $oDictionary, $vKey)

	If $oDictionary.Exists($vKey) Then $oDictionary.Remove($vKey)

EndFunc   ;==>_Dictionary_RemoveSafe


; инициализация/обновление счётчика в заданном ключе; малополезная фигня

Func _Dictionary_CounterIncrease(ByRef $oDictionary, $vKey)

	If Not $oDictionary.Exists($vKey) Then

		$oDictionary.Item($vKey) = 1

	Else
		$oDictionary.Item($vKey) += 1

	EndIf
EndFunc   ;==>_Dictionary_CounterIncrease


; перевод словаря в простой двухколоночный массив

Func _Dictionary_ToArray(ByRef $aRes, ByRef Const $oDictionary, $bAddCounter = False)

	Local $iUbound = $oDictionary.Count
	If $bAddCounter Then $iUbound += 1

	Local $iStart = Not $bAddCounter ? 0 : 1

	Dim $aRes[$iUbound][2]

	Local $aKeys = $oDictionary.Keys()
	Local $aItems = $oDictionary.Items()

	For $i = $iStart To $iUbound - 1

		$aRes[$i][0] = $aKeys[$i - $iStart]
		$aRes[$i][1] = $aItems[$i - $iStart]
	Next

	$aKeys = 0
	$aItems = 0

	If $bAddCounter Then $aRes[0][0] = $iUbound - 1

EndFunc   ;==>_Dictionary_ToArray

#EndRegion ; класс словаря
